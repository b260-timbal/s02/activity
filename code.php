<?php

// Divisible of Five Loop

function divisibleOfFive() {
    for($number = 0; $number <= 1000; $number++) {
        if($number % 5 !== 0) {
            continue;
        }
        echo $number. ",";
        if($number === 1000){
            break;
        }
    }
}

//Arrays
$students =[];